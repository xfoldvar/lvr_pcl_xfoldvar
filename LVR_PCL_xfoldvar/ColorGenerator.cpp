//
//  ColorGenerator.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 29/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "ColorGenerator.hpp"
ColorGenerator::ColorGenerator(){
    this->r = rand() % 255;
    this->g = rand() % 255;
    this->b = rand() % 255;
}

ColorGenerator::ColorGenerator(int r, int g, int b){
    this->r = r;
    this->g = g;
    this->b = b;
}

ColorStruct ColorGenerator::getColorStruct(){
    return ColorStruct(this->r, this->g, this->b);
}

int ColorGenerator::random(){
    srand (time(NULL));
    return rand() % 255 + 1;
}
