//
//  PLYFileLoader.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 28/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef PLYFileLoader_hpp
#define PLYFileLoader_hpp

#include <stdio.h>
#include <pcl/io/ply_io.h>
#include <string>

//Trieda pre nacitavanie zdrojoveho suboru s datami
class PLYFileLoader {
private:
    std::string m_defaultFile;
public:
    PLYFileLoader(std::string defaultFileName);
    pcl::PointCloud<pcl::PointXYZ>::Ptr loadCloud(std::string filename);
    std::string getDefaultFilePath();
};

#endif /* PLYFileLoader_hpp */
