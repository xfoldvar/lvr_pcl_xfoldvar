//
//  ColorStruct.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 28.01.18.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef ColorStruct_hpp
#define ColorStruct_hpp

#include <stdio.h>

struct ColorStruct {
    float r,g,b,a;
    ColorStruct(float r, float g, float b, int a = 0){
        this->r = r/255;
        this->g = g/255;
        this->b = b/255;
        this->a = a;
    }
};

#endif /* ColorStruct_hpp */
