//
//  GroundDetector.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 8.01.18.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "GroundDetector.hpp"

//Implementacia konstruktoru detektora zeme
GroundDetector::GroundDetector(pcl::PointCloud<pcl::PointXYZ>::Ptr input){
    //Vytvorime si zem
    pcl::PointCloud<pcl::PointXYZ>::Ptr ground(new pcl::PointCloud<pcl::PointXYZ>);
    //Vytvorime si vsetko ostatne
    pcl::PointCloud<pcl::PointXYZ>::Ptr everythingElse(new pcl::PointCloud<pcl::PointXYZ>);
    //Vytvorime odfiltrovanu zem
    pcl::PointIndicesPtr filteredGround(new pcl::PointIndices);
    //Inicializujeme filter
    pcl::ProgressiveMorphologicalFilter<pcl::PointXYZ> progressiveMorphFilter;
    //Nastavime filtru vstup
    progressiveMorphFilter.setInputCloud(input);
    //extrahujeme
    progressiveMorphFilter.extract(filteredGround->indices);
    //vytvorime extractor
    pcl::ExtractIndices<pcl::PointXYZ> extractor;
    //nastavime extractor
    extractor.setInputCloud(input);
    extractor.setIndices(filteredGround);
    extractor.filter(*everythingElse);
    extractor.setNegative(true);
    extractor.filter(*ground);
    //vysledok nasypeme do private premennych
    this->m_ground = ground;
    this->m_everythingElse = everythingElse;
}

//Vracia zem
pcl::PointCloud<pcl::PointXYZ>::Ptr GroundDetector::getGround(){
    return  this->m_ground;
}
//Vracia vsetko ostatne
pcl::PointCloud<pcl::PointXYZ>::Ptr GroundDetector::getEverythingElse(){
    return this->m_everythingElse;
}
