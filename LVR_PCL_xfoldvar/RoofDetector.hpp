//
//  RoofDetector.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 19/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef RoofDetector_hpp
#define RoofDetector_hpp

#include <stdio.h>
//pouzijeme BodyDetector
#include "BodyDetector.hpp"
//a vizualizer -- ten este nieje impl.
#include "Visualizer.hpp"
//z pcl potrebujeme point_cloud a io
#include <pcl/point_cloud.h>
#include <pcl/io/ply_io.h>

class RoofDetector{
private:
    pcl::PointCloud<pcl::PointXYZ>::Ptr m_roof, m_bottom, m_top;
    pcl::PointCloud<pcl::PointXYZ>::Ptr getRoofCloud();
    pcl::PointCloud<pcl::PointXYZ>::Ptr getTopCloud();
    pcl::PointCloud<pcl::PointXYZ>::Ptr getBottomCloud();
    pcl::PointCloud<pcl::PointXYZ>::Ptr getSimplifiedTop(pcl::PointCloud<pcl::PointXYZ>::Ptr data);
    pcl::PointCloud<pcl::PointXYZ>::Ptr getSimplifiedBottom(pcl::PointCloud<pcl::PointXYZ>::Ptr data);
    pcl::PointCloud<pcl::PointXYZ>::Ptr getSimplifiedRoof(pcl::PointCloud<pcl::PointXYZ>::Ptr data);
    float findMaxZ();
    float findMinZ();
    void moveRoofToZeroZ(pcl::PointCloud<pcl::PointXYZ>::Ptr input);
    pcl::PointCloud<pcl::PointXYZ>::Ptr transformPolygon(std::vector<cv::Point> polygon, float averageZ);
public:
    static const float tolerance;
    static const int min;
    static const int max;
    RoofDetector(pcl::PointCloud<pcl::PointXYZ>::Ptr input);
    void prepareToVisualizer(Visualizer *vis);
};
#endif /* RoofDetector_hpp */
