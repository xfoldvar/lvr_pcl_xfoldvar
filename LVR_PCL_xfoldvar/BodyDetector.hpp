//
//  BodyDetector.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 6.01.18.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef BodyDetector_hpp
#define BodyDetector_hpp

#include <stdio.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
//dalsie pcl classy
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
//filtre
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
//segmentaciu
#include <pcl/segmentation/sac_segmentation.h>
//a na detekciu trupu hull
#include <pcl/surface/concave_hull.h>
//pre zjednodusenie z cvika potrebujeme opencv takze
#include <opencv2/opencv.hpp>
class BodyDetector {
public:
    pcl::PointCloud<pcl::PointXYZ>::Ptr bodyToCloud
                    (pcl::PointCloud<pcl::PointXYZ>::Ptr input);
    pcl::PointCloud<pcl::PointXYZ>::Ptr makeBodySimple
                    (pcl::PointCloud<pcl::PointXYZ>::Ptr body);
};


#endif /* BodyDetector_hpp */
