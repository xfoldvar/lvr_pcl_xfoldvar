//
//  ColorGenerator.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 29/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef ColorGenerator_hpp
#define ColorGenerator_hpp

#include <stdio.h>
#include "ColorStruct.hpp"
#include <cstdlib>
#include <time.h>

class ColorGenerator{
public:
    ColorGenerator();
    ColorGenerator(int r, int g, int b);
    ColorStruct getColorStruct();
    int random();
private:
    int r,g,b;
};

#endif /* ColorGenerator_hpp */
