//
//  Engine.hpp
//  LVR_PCL_xfoldvar
//
//  Created by Adam Földvári on 29/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef Engine_hpp
#define Engine_hpp

#include <stdio.h>
#include "PLYFileLoader.hpp"
#include "GroundDetector.hpp"
#include "RoofDetector.hpp"
#include "RoofExtractor.hpp"
#include "Visualizer.hpp"
class Engine{
public:
    void run();
};

#endif /* Engine_hpp */
