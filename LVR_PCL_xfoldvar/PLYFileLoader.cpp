//
//  PLYFileLoader.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 28/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "PLYFileLoader.hpp"

PLYFileLoader::PLYFileLoader(std::string defaultFileName) {
    this->m_defaultFile = defaultFileName;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr PLYFileLoader::loadCloud(std::string filename) {
    //inicializujeme reader
    pcl::PLYReader reader;
    
    //inicializujeme cloud do ktoreho sa nasypu data zo suboru
    pcl::PointCloud<pcl::PointXYZ>::Ptr outputCloud(new pcl::PointCloud<pcl::PointXYZ>);
    //nacitame data
    reader.read(this->getDefaultFilePath(), *outputCloud);
    //vratime cloud
    return outputCloud;
}

std::string PLYFileLoader::getDefaultFilePath(){
    return this->m_defaultFile;
}
