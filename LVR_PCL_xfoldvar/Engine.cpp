//
//  Engine.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 30/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "Engine.hpp"


void Engine::run(){
    /**
     Z cvika pomocou ground detectoru si vytiahnem podstavy.
     Roof Extractor pouziva K-dimenzionalny strom pre vyhladavanie najblizsich bodov
     z toho mi vypadne segmentovane mracno bodov ktore predam Roof Detectoru
     V Roof Detectore sa pouziva BodyDetector kt. pouziva concavehull z cvika,  
     pomocou BD zjednodusim obdobne ako z cvika
     Nasledne pouzijem v RD metodu na zjednodusenie ktora mi vytvori jednotlive polygony.
     Na tvorbu kontury pouzijem z cvika opencv approxPolyDp.
     nasledne spocitam priemernu vysku bodu Z v cloude a pomocou nej vytvorim 3rozmerny bod
     Predam visualizeru a vo visualizeri prejdem jednotlive body a nakreslim knim ciary
     Kazdy segment je vykresleny rovnakou farbou.
     **/
    std::cout << "Run initialized" << std::endl;
    
    std::cout << "Creating visualizer" << std::endl;
    Visualizer *vis = new Visualizer();
    vis->init();
    std::cout << "Visualizer initialized" << std::endl;
    
    std::cout << "Loading PLY file ... ";
    PLYFileLoader *fileLoader = new PLYFileLoader("./data/Urban_small.ply");
    std::cout << "done" << std::endl;
    std::cout << "Creating cloud ... ";
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = fileLoader->loadCloud(fileLoader->getDefaultFilePath());
    std::cout << "done" << std::endl;
    
    std::cout << "Initializing ground detection ... ";
    GroundDetector *gd = new GroundDetector(cloud);
    std::cout << "done" << std::endl;
    pcl::PointCloud<pcl::PointXYZ>::Ptr ground = gd->getGround();
    
    std::cout << "Initializing roof extractor ... ";
    RoofExtractor *re = new RoofExtractor();
    std::cout << "done" << std::endl;
    std::cout << "Creating roof cloud segments ... ";
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> segmentedClouds = re->createRoofSegments(ground);
    std::cout << "done" << std::endl;
    
    for(auto segment : segmentedClouds){
        std::cout << "Adding cloud segment ... ";
        RoofDetector *rd = new RoofDetector(segment);
        rd->prepareToVisualizer(vis);
        std::cout << "done" << std::endl;
    }
    
    std::cout << "Running visualization ... ";
    vis->run();
    std::cout << "done" << std::endl;
}
