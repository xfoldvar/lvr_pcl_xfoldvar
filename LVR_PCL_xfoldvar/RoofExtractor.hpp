//
//  RoofExtractor.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 22/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef RoofExtractor_hpp
#define RoofExtractor_hpp

#include <stdio.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/search/kdtree.h>

class RoofExtractor {
public:
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> createRoofSegments(pcl::PointCloud<pcl::PointXYZ>::Ptr input);
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> computeSegments(pcl::PointCloud<pcl::PointXYZ>::Ptr input,std::vector<pcl::PointIndices> pointIndices);
    static const float clusterTolerance;
    static const float minClusterSize;
    static const float maxClusterSize;
};

#endif /* RoofExtractor_hpp */
