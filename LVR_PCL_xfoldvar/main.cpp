#include <iostream>
#include "Engine.hpp"

int main(int argc, const char * argv[])
{
    Engine *engine = new Engine();
    engine->run();
    return 0;
}
