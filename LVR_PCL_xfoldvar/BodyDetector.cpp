//
//  BodyDetector.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 6.01.18.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "BodyDetector.hpp"
//Z bodov vytvorime PointCloud
//sorry neviem preco mi to takto zalamuje
pcl::PointCloud<pcl::PointXYZ>::Ptr BodyDetector::bodyToCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr input){
    pcl::ConcaveHull<pcl::PointXYZ> body;
    body.setInputCloud(input);
    body.setAlpha(50);
    body.setDimension(2);
    pcl::PointCloud<pcl::PointXYZ>::Ptr bodyCloud (new pcl::PointCloud<pcl::PointXYZ>);
    body.reconstruct(*bodyCloud);
    
    return bodyCloud;
}
//Zjednodusi vstupny cloud
//Robili sme na cviku takze pouzijem zjednodusenie
pcl::PointCloud<pcl::PointXYZ>::Ptr BodyDetector::makeBodySimple(pcl::PointCloud<pcl::PointXYZ>::Ptr input){
    pcl::PointCloud<pcl::PointXYZ>::Ptr simpleBody (new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<cv::Point2f> points;
    for(auto point : *input){
        cv::Point2f *cvPoint = new cv::Point2f();
        cvPoint->y = point.y;
        cvPoint->x = point.x;
        points.push_back(*cvPoint);
    }
    std::vector<cv::Point> polygon;
    cv::approxPolyDP(points, polygon, 5, true);
    for(auto point : polygon){
        pcl::PointXYZ resPoint;
        resPoint.y = point.y;
        resPoint.x = point.x;
        simpleBody->points.push_back(resPoint);
    }
    return simpleBody;
}
