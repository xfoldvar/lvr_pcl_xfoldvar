//
//  RoofDetector.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 19/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "RoofDetector.hpp"

//PUBLIC SCOPE

//konstruktor
RoofDetector::RoofDetector(pcl::PointCloud<pcl::PointXYZ>::Ptr input){
    BodyDetector *bd = new BodyDetector();
    pcl::PointCloud<pcl::PointXYZ>::Ptr bottom,top,bodyCloud;
    this->m_roof = input;
    bodyCloud = bd->bodyToCloud(input);
    bottom = bd->makeBodySimple(bodyCloud);
    top = this->getSimplifiedTop(this->getTopCloud());
    this->m_top = top;
    this->m_roof = input;
    this->m_bottom = bottom;
}

void RoofDetector::prepareToVisualizer(Visualizer *vis){
    //vektor s jednotlivymi body-ckami
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> bodies;
    //pridame podstavy
    bodies.push_back(this->m_bottom);
    //najkratsie hrany
    bodies.push_back(this->m_top);
    //TODO implementovat metodu vo vizualizeri a poslat bodies do vizualizeru
    vis->addBodies(bodies);
}

//Konstanty - keby sme ich potrebovali menit
const float RoofDetector::tolerance = 0.188;
const int RoofDetector::min =  999999999;
const int RoofDetector::max = -999999999;

//PRIVATE SCOPE

float RoofDetector::findMaxZ(){
    float maxZ = RoofDetector::max;
    for (auto targetPoint : m_roof->points) {
        if (targetPoint.z > maxZ) {
            maxZ = targetPoint.z;
        }
    }
    return maxZ;
}

float RoofDetector::findMinZ(){
    float minZ = RoofDetector::min;
    for (auto targetPoint : m_roof->points) {
        if (targetPoint.z < minZ) {
            minZ = targetPoint.z;
        }
    }
    return minZ;
}

void RoofDetector::moveRoofToZeroZ(pcl::PointCloud<pcl::PointXYZ>::Ptr input){
    pcl::PointCloud<pcl::PointXYZ>::Ptr newCloud (new pcl::PointCloud<pcl::PointXYZ>);
    float minZ,maxZ;
    maxZ = findMaxZ();
    minZ = findMinZ();
    for (auto targetPoint : this->m_roof->points) {
        targetPoint.z += 0 - minZ;
        newCloud->points.push_back(targetPoint);
    }
    this->m_roof = newCloud;
}

//Vratime si top cloud
pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::getTopCloud(){
    //pomocna premenna do ktorej ulozime najvyssie body
    pcl::PointCloud<pcl::PointXYZ>::Ptr topPointsStorage (new pcl::PointCloud<pcl::PointXYZ>);
    //pri vyhladavani potrebujeme najst najnizsie a najvyssie body
    float maxZ,minZ,tempTolerance,zBellowZero;
    maxZ = findMaxZ();
    minZ = findMinZ();
    tempTolerance = maxZ - tolerance;
    zBellowZero = 0 - minZ;
    //a ideme hladat najvyssie body
    for (auto targetPoint : this->getRoofCloud()->points) {
        if (targetPoint.z > tempTolerance) {
            targetPoint.z = targetPoint.z + zBellowZero;
            topPointsStorage->points.push_back(targetPoint);
        }
    }
    //posunut na 0
    this->moveRoofToZeroZ(this->m_roof);
    
    return topPointsStorage;
}

//Vratime si bottom cloud
pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::getBottomCloud(){
    return this->m_bottom;
}

//Vratime si roof cloud
pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::getRoofCloud(){
    return this->m_roof;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::transformPolygon(std::vector<cv::Point> polygon, float averageZ){
    pcl::PointCloud<pcl::PointXYZ>::Ptr result (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointXYZ resultPoint;
    for (auto point : polygon) {
        resultPoint.x = point.x;
        resultPoint.y = point.y;
        resultPoint.z = averageZ;
        result->points.push_back(resultPoint);
    }
    return result;
}

//Zjednodusenie - obdobne tomu z cvicenia

//TOP
pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::getSimplifiedTop(pcl::PointCloud<pcl::PointXYZ>::Ptr data){
    float maxZ,minZ,averageZ;
    maxZ = findMaxZ();
    minZ = findMinZ();
    averageZ = (minZ - maxZ)/2;
    //docasna premenna
    std::vector<cv::Point2f> input;
    for (auto targetPoint : *data) {
        cv::Point2f *convertedPoint = new cv::Point2f();
        convertedPoint->x = targetPoint.x;
        convertedPoint->y = targetPoint.y;
        input.push_back(*convertedPoint);
    }
    std::vector<cv::Point> polygon;
    cv::approxPolyDP(input, polygon, 1, true);

    return this->transformPolygon(polygon, averageZ);
}

//BOTTOM
pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::getSimplifiedBottom(pcl::PointCloud<pcl::PointXYZ>::Ptr data){
    //Asi nebude potrebne implementovat
    return this->m_bottom;
}

//ROOF
pcl::PointCloud<pcl::PointXYZ>::Ptr RoofDetector::getSimplifiedRoof(pcl::PointCloud<pcl::PointXYZ>::Ptr data){
    //Asi tiez nebude potrebne implementovat
    return this->m_roof;
}
