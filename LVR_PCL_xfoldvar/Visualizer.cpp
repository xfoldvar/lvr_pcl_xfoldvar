//
//  Visualizer.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 29/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "Visualizer.hpp"

//V konstruktore nasetujeme prazdne hodnoty
Visualizer::Visualizer(){
    this->m_cloudCounter = 0;
    this->m_linesCounter = 0;
    //this->m_viewer = NULL;
}

//inicializacia
void Visualizer::init(){
    //vytvorime viewer
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("LVR_PCL_VISUALIZER"));
    //mnastavime cierne pozadie
    viewer->setBackgroundColor(0, 0, 0);
    //nasetujeme
    this->m_viewer = viewer;
}

//spustenie vizualizacie
void Visualizer::run(){
    //Vieweru nastavime parametre
    this->m_viewer->initCameraParameters();
    this->m_viewer->resetCamera();
    //a v cykle vykonavame
    while(!this->m_viewer->wasStopped()){
        this->m_viewer->spinOnce(100);
        boost::this_thread::sleep(boost::posix_time::microseconds(100000));
    }
    //koniec
    this->m_viewer->close();
}

//pridame cloud
void Visualizer::addCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr data){
    ColorGenerator *cg = new ColorGenerator();
    ColorStruct cs = cg->getColorStruct();
    std::stringstream str;
    str << "cloud" << this->m_cloudCounter;
    std::string name = str.str();
    //pridame do vizualizeru s propertami
    this->m_viewer->addPointCloud(data, name);
    this->m_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1.0, name);
    this->m_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, cs.r, cs.g, cs.b, name);
}

//Hromadne pridanie mracien
void Visualizer::addClouds(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> data){
    for(auto &cloud : data){
        this->addCloud(cloud);
    }
}

//Pridavanie trupov
void Visualizer::addBodies(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> data){
    //Vygenerujeme farbu
    ColorGenerator *cg = new ColorGenerator();
    ColorStruct cs = cg->getColorStruct();
    std::cout << cs.r << endl;
    //Pre kazdy cloud generujeme ciary, jednotlive ciary sa nasledne prepajaju
    for (auto &cloud : data){
        for (int i = 0; i < cloud->points.size()-1; i++) {
            std::stringstream str;
            str << "line" << this->m_linesCounter;
            std::string name = str.str();
            this->m_linesCounter++;
            this->m_viewer->addLine(cloud->points.at(i),
                                    cloud->points.at(i+1),
                                    name);
            m_viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, cs.r, cs.g, cs.b, name);
        }
        std::stringstream str;
        str << "line" << this->m_linesCounter;
        std::string name = str.str();
        this->m_linesCounter++;
        this->m_viewer->addLine(cloud->points.at(cloud->points.size()-1), cloud->points.at(0), name);
        this->m_viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, cs.r, cs.g, cs.b, name);
    }
    
    if (data.size() == 2) {
        pcl::PointCloud<pcl::PointXYZ>::Ptr bottom = data.at(0);
        pcl::PointCloud<pcl::PointXYZ>::Ptr top = data.at(1);
        //v cykle pridavame trupy
        for (int i = 0; i < bottom->points.size(); i++) {
            this->addBody(top,bottom,i, cs);
        }
    }
    this->m_cloudCounter++;
}

void Visualizer::addBody(pcl::PointCloud<pcl::PointXYZ>::Ptr top, pcl::PointCloud<pcl::PointXYZ>::Ptr bottom, int iterator, ColorStruct cs){
    pcl::PointXYZ minDistanceFromTop = top->points.at(0);
    for (int j = 0; j < top->points.size(); j++) {
        if (pcl::geometry::distance(bottom->points.at(iterator), top->points.at(j)) < pcl::geometry::distance(bottom->points.at(iterator), minDistanceFromTop)) {
            minDistanceFromTop = top->points.at(j);
        }
    }
    std::stringstream str;
    str << "line" << this->m_linesCounter;
    std::string name = str.str();
    this->m_linesCounter++;
    m_viewer->addLine(bottom->points.at(iterator), minDistanceFromTop, name);
    m_viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, cs.r, cs.g, cs.b, name);
}



