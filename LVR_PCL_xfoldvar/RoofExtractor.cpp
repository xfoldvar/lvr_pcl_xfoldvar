//
//  RoofExtractor.cpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 22/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#include "RoofExtractor.hpp"

const float RoofExtractor::clusterTolerance = 1.2;
const float RoofExtractor::minClusterSize = 299;
const float RoofExtractor::maxClusterSize = 26500;

std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> RoofExtractor::createRoofSegments(pcl::PointCloud<pcl::PointXYZ>::Ptr input){
    //extraktor z PCLka pracuje s klustrami potrebuje nejaku search metodu
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> extractor;
    //pouzijeme k-dimnezionalny strom kt sluzi ako vyhladavacia metoda v extraktore
    pcl::search::KdTree<pcl::PointXYZ>::Ptr kdtree (new pcl::search::KdTree<pcl::PointXYZ>);
    //stromu nasetujeme vstupny cloud
    kdtree->setInputCloud(input);
    //extractoru predame pripraveny strom
    extractor.setSearchMethod(kdtree);
    //extractoru nasetujeme vstupne data
    extractor.setInputCloud(input);
    //nastavime parametre extraktoru podla dokumentacie
    extractor.setClusterTolerance(RoofExtractor::clusterTolerance);
    extractor.setMaxClusterSize(RoofExtractor::maxClusterSize);
    extractor.setMinClusterSize(RoofExtractor::minClusterSize);
    //vytvorime si vektor indicie do ktorych extraktor nasype body
    std::vector<pcl::PointIndices> pointIndices;
    //nasypeme to tam z extraktoru
    extractor.extract(pointIndices);
    //vratime vysledok
    return this->computeSegments(input, pointIndices);
}

std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> RoofExtractor::computeSegments(pcl::PointCloud<pcl::PointXYZ>::Ptr input,std::vector<pcl::PointIndices> pointIndices){
    std::cout << "COMPUTING SEGMENTS" << std::endl;
    //vytvorime temp premennu vysledku zvysok je peklo s iteratormi, nasiel som to na nete ako sa snimi pracuje
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> result;
    //pre vsetky body ktore boli vyextractovane vytvorime clustre
    for (std::vector<pcl::PointIndices>::const_iterator iterator = pointIndices.begin (); iterator != pointIndices.end (); ++iterator) {
        //inicializujeme si prazdny cluster cloud
        pcl::PointCloud<pcl::PointXYZ>::Ptr cluster (new pcl::PointCloud<pcl::PointXYZ>);
        //v cykle budeme pridavat body
        for (std::vector<int>::const_iterator another_iterator = iterator->indices.begin (); another_iterator != iterator->indices.end (); ++another_iterator) {
            //clustru vlozime body
            cluster->points.push_back(input->points[*another_iterator]);
        }
        //vysledny cluster este upravime
        cluster->height = 1;
        long clusterSize = cluster->points.size();
        cluster->width = (uint32_t)clusterSize;
        cluster->is_dense = true;
        //a pushneme do resultu
        result.push_back(cluster);
    }
    return result;
}
