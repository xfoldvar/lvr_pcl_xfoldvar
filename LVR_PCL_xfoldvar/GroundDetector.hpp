//
//  GroundDetector.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 8.01.18.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef GroundDetector_hpp
#define GroundDetector_hpp

#include <stdio.h>
//musime includnut indicie
#include <pcl/filters/extract_indices.h>
//a taktiez progresivny morfologicky filter
#include <pcl/segmentation/progressive_morphological_filter.h>
class GroundDetector{
public:
    GroundDetector(pcl::PointCloud<pcl::PointXYZ>::Ptr input);
    pcl::PointCloud<pcl::PointXYZ>::Ptr getGround();
    pcl::PointCloud<pcl::PointXYZ>::Ptr getEverythingElse();
    
private:
    pcl::PointCloud<pcl::PointXYZ>::Ptr m_ground;
    pcl::PointCloud<pcl::PointXYZ>::Ptr m_everythingElse;
};

#endif /* GroundDetector_hpp */
