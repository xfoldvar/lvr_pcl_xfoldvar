//
//  Visualizer.hpp
//  LVR_PCL_xfoldvar
//
//  Created by xfoldvar on 29/01/2018.
//  Copyright © 2018 xfoldvar. All rights reserved.
//

#ifndef Visualizer_hpp
#define Visualizer_hpp

#include <stdio.h>
#include "ColorGenerator.hpp"
#include <boost/thread/thread.hpp>
#include <pcl/PolygonMesh.h>
#include <pcl/common/geometry.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/geometry/planar_polygon.h>

class Visualizer{
public:
    Visualizer();
    void init();
    void run();
    void addBodies(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> data);
    void addCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr data);
    void addClouds(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> data);
    void addBody(pcl::PointCloud<pcl::PointXYZ>::Ptr top, pcl::PointCloud<pcl::PointXYZ>::Ptr bottom, int iterator, ColorStruct cs);
private:
    int m_cloudCounter, m_linesCounter;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> m_viewer;
};

#endif /* Visualizer_hpp */
